<?php

namespace Project\Onclick;

use cUser,
    CSaleBasket,
    Bitrix\Main,
    Bitrix\Main\Application,
    Bitrix\Main\Loader,
    Bitrix\Main\Context,
    Bitrix\Main\Config\Option,
    Bitrix\Main\CurrencyManager,
    Bitrix\Sale,
    Bitrix\Sale\DiscountCouponsManager;

class Order {

    static public function create($arOrder) {
        $siteId = Context::getCurrent()->getSite();
        $currencyCode = Option::get('sale', 'default_currency', 'RUB');
        DiscountCouponsManager::init();
        $registeredUserID = cUser::GetID() ?: Config::ONECLICK_USER_ID;
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $siteId);

        if (0) {
            $basket = Sale\Basket::loadItemsForFUser(CSaleBasket::GetBasketUserID(), Context::getCurrent()->getSite())->getOrderableItems();
            if ($item = $basket->getExistsItem('catalog', 73)) {
                $item->setField('QUANTITY', $item->getQuantity() + $quantity);
            } else {
                $item = $basket->createItem('catalog', 73);
                $item->setFields(array(
                    'QUANTITY' => 1,
                    'CURRENCY' => CurrencyManager::getBaseCurrency(),
                    'LID' => Context::getCurrent()->getSite(),
                    'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                ));
            }
            $basket->save();
        }

        $order = Sale\Order::create($siteId, $registeredUserID);
        $order->setPersonTypeId(Config::PERSON_TYPE_ID);
        $basket = Sale\Basket::loadItemsForFUser(CSaleBasket::GetBasketUserID(), Context::getCurrent()->getSite())->getOrderableItems();
        $order->setBasket($basket);

        $propertyCollection = $order->getPropertyCollection();
        $propertyCollection->getItemByOrderPropertyId(1)->setValue($arOrder['NAME']);
        $propertyCollection->getItemByOrderPropertyId(2)->setValue($arOrder['PHONE']);
        $propertyCollection->getItemByOrderPropertyId(3)->setValue($arOrder['EMAIL']);

        $order->setField('USER_DESCRIPTION', $arOrder['COMMENT']);

        /* Shipment */
//        $shipmentCollection = $order->getShipmentCollection();
//        $shipment = $shipmentCollection->createItem();
//        $shipment->setFields(array(
//            'DELIVERY_ID' => 4,
//            'DELIVERY_NAME' => 'Самовывоз',
//            'CURRENCY' => $order->getCurrency()
//        ));
//
//        $shipmentItemCollection = $shipment->getShipmentItemCollection();
//        foreach ($order->getBasket() as $item) {
//            $shipmentItem = $shipmentItemCollection->createItem($item);
//            $shipmentItem->setQuantity($item->getQuantity());
//        }

        /* Payment */
//        $paymentCollection = $order->getPaymentCollection();
//        $extPayment = $paymentCollection->createItem();
//        $extPayment->setFields(array(
//            'PAY_SYSTEM_ID' => 3,
//            'PAY_SYSTEM_NAME' => 'Наличные',
//            'SUM' => $order->getPrice()
//        ));

        /**/
        $order->doFinalAction(true);

//        $propertyCollection = $order->getPropertyCollection();
//
//
//        foreach ($propertyCollection->getGroups() as $group) {
//
//            foreach ($propertyCollection->getGroupProperties($group['ID']) as $property) {
//
//                $p = $property->getProperty();
//                if ($p["CODE"] == "CONTACT_PERSON")
//                    $property->setValue("VASYA");
//            }
//        }

        $order->setField('CURRENCY', $currencyCode);
        $order->setField('COMMENTS', 'Заказ в 1 клик' );
        $order->save();
        return $order->GetId();
    }

}
