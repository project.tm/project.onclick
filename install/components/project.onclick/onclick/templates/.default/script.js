$(function () {

    $("#edit-commerce-buy-one-click-button--3").fancybox({
        type: 'inline',
        href: '.commerce-buy-one-click-form'
    });

    $(document).on('click', '.commerce-buy-one-click-form #edit-cancel', function () {
        $.fancybox.close(true);
    });

    if (oneClickElement) {
        oneClickElement.isOneClick = false;
        if(!oneClickElement.oneClickCountToBasket) {
            oneClickElement.oneClickCountToBasket = 1;
        }
        oneClickElement.oneClickBasketResult = oneClickElement.BasketResult;
        oneClickElement.BasketResult = function (arResult) {
            if (oneClickElement.isOneClick) {
                oneClickElement.countOneClick--;
                if (!oneClickElement.countOneClick) {
                    oneClickElement.isOneClick = false;
                    oneClickObject.sendPost(oneClickElement.elOneClick, $(oneClickElement.elOneClick).serialize());
                }
            } else {
                this.oneClickBasketResult(arResult);
            }
        };
    }

    $(document).on('submit', '#commerce-buy-one-click-form', function () {
        oneClickElement.elOneClick = this;
        oneClickElement.isOneClick = true;
        oneClickElement.countOneClick = oneClickElement.oneClickCountToBasket;
        oneClickElement.Add2Basket();
        return false;
    });

});